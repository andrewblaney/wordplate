<?php

function wpbootstrap_scripts_with_jquery()
{
	// Register the script like this for a theme:
	wp_register_script( 'custom-script', get_template_directory_uri() . '/bootstrap/js/bootstrap.js', array( 'jquery' ) );
	// For either a plugin or a theme, you can then enqueue the script:
	wp_enqueue_script( 'custom-script' );
}
add_action( 'wp_enqueue_scripts', 'wpbootstrap_scripts_with_jquery' );


if ( function_exists('register_sidebar') )
	register_sidebar(array(
        'name' => 'sidebar',
		'before_widget' => '',
		'after_widget' => '',
		'before_title' => '<h3>',
		'after_title' => '</h3>',
	));

require_once('wp_bootstrap_navwalker.php');

register_nav_menus( array(
    'primary' => __( 'menu', 'wpbootstrap' ),
) );

/**
 * Redirect non-admins to the homepage after logging into the site.
 *
 * @since 	1.0
 */
function acme_login_redirect( $redirect_to, $request, $user  ) {
	return ( is_array( $user->roles ) && in_array( 'administrator', $user->roles ) ) ? admin_url() : site_url();
}
add_filter( 'login_redirect', 'acme_login_redirect', 10, 3 );

add_action( 'register_form', 'extended_register_form', 10, 0 );

function remove_dashboard_widgets() {
    global $wp_meta_boxes;

    // hide wp news and activity
    unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);
    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_activity']);

}

add_action('wp_dashboard_setup', 'remove_dashboard_widgets' );

remove_action( 'admin_color_scheme_picker', 'admin_color_scheme_picker' );

function hide_personal_options(){
    echo "\n" . '<script type="text/javascript">jQuery(document).ready(function($) { $(\'form#your-profile > h3:first\').hide(); $(\'form#your-profile > table:first\').hide(); $(\'form#your-profile\').show(); });</script>' . "\n";
}
add_action('admin_head','hide_personal_options');


function my_custom_dashboard_widgets() {
    global $wp_meta_boxes;

    wp_add_dashboard_widget('custom_help_widget', '<h1>Welcome!</h1>', 'custom_dashboard_help');
}

function custom_dashboard_help() {
    echo '<h3>Welcome to Kihoutei!<br>
        This area is for <a href="http://kihoutei.com/wp-admin/profile.php">editing your profile details</a> only.<br>
         To return back to Kihoutei, <a href ="' . home_url(). '">click here</a>!
    </h3>';
}

add_action('wp_dashboard_setup', 'my_custom_dashboard_widgets');