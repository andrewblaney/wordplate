  <head>
    <meta charset="utf-8">
    <title>Kihoutei Beta</title>
      <div class="corner-ribbon top-right sticky green">beta</div>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Le styles -->
    <link href="<?php bloginfo('stylesheet_url');?>" rel="stylesheet">
      <script type="text/javascript" src="bootstrap/js/bootstrap.js" charset="UTF-8"></script>


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <?php wp_enqueue_script("jquery"); ?>
    <?php wp_head(); ?>
  </head>
  <?php include('nav.php');?>

  <?php
  if ( !( is_super_admin(get_current_user_id()) ) ) {
      show_admin_bar(false);
  } ?>

  <div class="container">


