<?php header('Access-Control-Allow-Origin: http://disqus.com'); ?>
<?php get_header();?>
<div class ='col-md-12'>


        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
            <?php

                echo "<h2>" . get_the_title($post->post_id) . "</h2>";
            ?>
            <?php the_content(); ?>


        <?php endwhile; else: ?>
            <p><?php _e('Sorry, this page does not exist.'); ?></p>
        <?php endif; ?>
</div>
<?php if (is_user_logged_in()) : ?>
<?php comments_template(); ?>
<?php endif ?>

<?php if (!(is_user_logged_in())) : ?>
<a href='/wp-login.php'>Login</a> or <a href="/wp-login.php?action=register">Register</a> to start the discussion!
<?php endif ?>


<?php get_footer(); ?>
